package Juego;

import java.util.Scanner;
import java.util.Random;
import Juego.Jugador;

public class Main {

	// Autor: Iker RU con colaboracion de AlbertTTT
	// Fecha: 27/11/20
	// Programa: Juego de los dados

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Random r = new Random();

//variables//////////////////////////////////////////////////	
//variables//////////////////////////////////////////////////
		int dau1;
		int dau2;
		int dau3;
		int dau4;
		int res;
		// int apuestaT = 0;
		int numerojugadores;
		String auxnom;
		String continuar;

//En este juego se tiraran cuatro dados de seis caras, el objetivo es adivinar el numero de la suma de los tres resultados.

//Introduccion de jugadores///////////////////////////////////////////////////
//Introduccion de jugadores///////////////////////////////////////////////////
		System.out.println("--------------------------------------");
		System.out.println("Juego de los cuatro dados!            ");
		System.out.println("Numero de jugadores:                  ");
		System.out.println("--------------------------------------");
		numerojugadores = sc.nextInt();
		sc.nextLine();
		Jugador Jugadores[] = new Jugador[numerojugadores];
		
//BUCLE DE JUEGO////////////////////////////////////////////////
//BUCLE DE JUEGO////////////////////////////////////////////////
		
//Bucle de creacion de jugadores	

		for (int i = 0; i < numerojugadores; i++) {

			System.out.println("Introduce jugador " + (i + 1) + ":");
			auxnom = sc.nextLine();

			Jugadores[i] = new Jugador(auxnom, 0, 0, 0);
		}

		do {

			Menu(Jugadores, numerojugadores, sc);

// Operacion de los dados 

			/*
			 * for(int j = 0; j < numerojugadores; j++) { apuestaT = apuestaT +
			 * Jugadores[j].apuesta; }
			 */
			dau1 = r.nextInt(6) + 1;
			dau2 = r.nextInt(6) + 1;
			dau3 = r.nextInt(6) + 1;
			dau4 = r.nextInt(6) + 1;
			res = dau1 + dau2 + dau3 + dau4;

			System.out.println("");
			System.out.println("--------------------------------------");
			System.out.println(" Dado 1:" + dau1 + "| Dado 2:" + dau2 + "| Dado 3:" + dau3 + "| Dado 4:" + dau4);

//Imprimir resultado de la suma de los dados

			System.out.println("            Total: " + res + "                 ");
			System.out.println(" ");

//Resultado de los jugadores

			Resultados(Jugadores, res, numerojugadores/* , apuestaT */);

			System.out.println("Continuar? (S(salir)) (C(continuar))");
			sc.nextLine();
			continuar = sc.nextLine();

			// System.out.println("comodin"+ comodin);
			// System.out.println("continuar"+ continuar);
		} while (!continuar.equals("s") && !continuar.equals("S"));

//Fin

		System.out.println("--------------------------------------");

		for (int k = 0; k < numerojugadores; k++) {

			System.out.println(Jugadores[k].nombre + " Tienes: " + Jugadores[k].puntos + " puntos.");

		}
		System.out.println();
		System.out.println("|--------------------------------------|");
		System.out.println("|Fin del juego.                        |");
		System.out.println("|Top:                                  |");
		System.out.println("|1: Albert	2V                     |");
		System.out.println("|1: Iker 	2V                     |");
		System.out.println("|--------------------------------------|");

		sc.close();

	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Menu de juego
	public static void Menu(Jugador jugadores[], int numjug, Scanner scan) {

		for (int i = 0; i < numjug; i++) {

			System.out.println("Apuesta de puntos de " + jugadores[i].nombre + " : ");
			jugadores[i].apuesta = scan.nextInt();
			System.out.println("Numero de la suma de los dados: ");
			jugadores[i].numero = scan.nextInt();
		}

	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Resultado de la ronda
	public static void Resultados(Jugador jugadores[], int resultado, int nj/* , int Total */) {

		// Imprimir resultado de la partida

		for (int i = 0; i < nj; i++) {

			if (jugadores[i].numero == resultado) {

				System.out.println(jugadores[i].nombre + " has ganado: " + /* Total */ jugadores[i].apuesta * 2 + "!");
				jugadores[i].puntos = jugadores[i].puntos + /* Total */jugadores[i].apuesta * 2;

			} else {
				System.out.println(jugadores[i].nombre + " has perdido: " + jugadores[i].apuesta + "!");
				jugadores[i].puntos = jugadores[i].puntos - jugadores[i].apuesta;

			}

		}

		System.out.println("--------------------------------------");
		System.out.println("");

	}

}
